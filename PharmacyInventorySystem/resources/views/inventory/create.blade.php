<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>
<body>
    <div class="container-sm">

        <h1 class="display-1">Pharmacy Inventory</h1>

        <form method="POST" action="/inventory">

            @csrf

            <label for="name" class="form-label">Name: </label>
            <input type="text" id="name" name="name" class="form-control">

            <label for="brand" class="form-label">Brand :</label>
            <input type="text" id="brand" name="brand" class="form-control">

            <label for="amount" class="form-label">Amount:</label>
            <input type="text" id="amount" name="amount" class="form-control">

            <label for="purpose" class="form-label">Purpose:</label>
            <input type="text" id="purpose" name="purpose" class="form-control">

            <div style="padding-top:10px;">

                <button class="btn btn-outline-primary">Create</button>
                <a href="/inventory" class="btn btn-outline-secondary">Go Back</a>

            </div>
            
        </form>
    </div>
</body>
</html>