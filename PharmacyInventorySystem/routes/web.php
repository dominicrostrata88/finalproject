<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PharmacyInventoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/inventory');
});

Route::get('/inventory', [PharmacyInventoryController::class, 'index']);
Route::post('/inventory', [PharmacyInventoryController::class, 'store']);
Route::put('/inventory/{inventory}', [PharmacyInventoryController::class, 'update']);
Route::get('/inventory/{inventory}/edit', [PharmacyInventoryController::class, 'edit']);
Route::get('/inventory/create', [PharmacyInventoryController::class, 'create']);
Route::delete('/inventory/{inventory}', [PharmacyInventoryController::class, 'destroy']);

Route::get('dashboard', [CustomAuthController::class, 'dashboard']); 
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registration', [CustomAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');