<?php

namespace App\Http\Controllers;

use App\Models\PharmacyInventory;
use Illuminate\Http\Request;

class PharmacyInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $inventory = PharmacyInventory::all();
       return view('inventory.index', ['inventory' => $inventory]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'name' => 'required',
            'brand' => 'required',
            'amount' => 'required',
            'purpose' => 'required',
        ]);

        PharmacyInventory::create([
            'name' => request('name'),
            'brand' => request('brand'),
            'amount' => request('amount'),
            'purpose' => request('purpose'),
        ]);

        return redirect('/inventory');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PharmacyInventory  $pharmacyInventory
     * @return \Illuminate\Http\Response
     */
    public function show(PharmacyInventory $pharmacyInventory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PharmacyInventory  $pharmacyInventory
     * @return \Illuminate\Http\Response
     */
    public function edit(PharmacyInventory $inventory)
    {
        return view('inventory.edit', ['inventory'=>$inventory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PharmacyInventory  $pharmacyInventory
     * @return \Illuminate\Http\Response
     */
    public function update(PharmacyInventory $inventory)
    {
        request()->validate([
            'name' => 'required',
            'brand' => 'required',
            'amount' => 'required',
            'purpose' => 'required',
        ]);
        $inventory->update([
            'name' => request('name'),
            'brand' => request('brand'),
            'amount' => request('amount'),
            'purpose' => request('purpose'),
        ]);

        return redirect('/inventory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PharmacyInventory  $pharmacyInventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PharmacyInventory $inventory)
    {
        $inventory->delete();

        return redirect('/inventory');
    }
}
